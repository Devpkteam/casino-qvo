<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Casino extends MY_Controller {

    public $data = null;
    public $usuario = null;

    public function __construct() {
        parent::__construct();
        $this->load->model('modelo_universal');
        $this->load->helper('cookie');
        $this->load->library('session');
    }

    public function index() {
//        $role = parent::verify_role();
//        if($role == true){
//        debug($this->input->cookie('token'));
//        debug($this->last_hour());
        if (($this->input->cookie('token', true) != false)) {
            $this->token_cokie();
        }
//        debug($this->session->userdata('id_role'));
        if ($this->session->userdata('id_role') == 1) {
            redirect('./dashboard');
        } elseif ($this->session->userdata('id_role') == 2) {
            redirect('./account');
        } elseif ($this->session->userdata('id_role') == false) {
            parent::index();
        }
//    }
    }

    public function restoremoney($id){
        $role = parent::verify_role();
        if ($role == true) {
            $select =$this->modelo_universal->select('temp_bet','*',array('id_game'=>$id));
            foreach($select as $s){
                //$this->modelo_universal->query('UPDATE user_data SET coins = (coins +'.$s['coins_game'].') WHERE id_user ='. $s['id_user']);
                if(isset($_GET['free'])){
                    $this->db->query('UPDATE `user_data` SET `coinsfree` = (`coinsfree` + '.$s['coins_game'].') WHERE `id_user` ='. $s['id_user']);
                }
                else{
                    $this->db->query('UPDATE `user_data` SET `coins` = (`coins` + '.$s['coins_game'].') WHERE `id_user` ='. $s['id_user']);
                }
                
                $select =$this->modelo_universal->delete('temp_bet',array('id_game'=>$id,'id_temp_bet'=>$s['id_temp_bet']));
          
            }
        }
    }

    public function dashboard() {
        $role = parent::verify_role();
        if ($role == true) {
//            $this->load->view('page/header');
            $jackpot= $this->modelo_universal->query('SELECT  ROUND(SUM(`debt`), 2) as jackpot FROM `casino_jackpot`');
            //debug($jackpot[0]['jackpot']);
            $this->data['jackpots']=$jackpot[0]['jackpot'];
            $recent_payments = $this->modelo_universal->count('register_payment', 'register_payment_status_id = 1');
            //debug($recent_payments);
            $this->data['recent_payments'] = $recent_payments;
            $active_users = $this->modelo_universal->count('user', 'id_user_account_status = 3');
            //debug($recent_payments);
            $this->data['active_users'] = $active_users;

            $reload = $this->modelo_universal->query('SELECT * FROM `register_payment`, `register_payment_status`, `user` WHERE `register_payment_status`.`id_register_payment_status`= 1 and `register_payment_status`.`id_register_payment_status`=`register_payment`.`register_payment_status_id` and `register_payment`.`id_user`= `user`.`id_user`  AND `register_payment`.`amount` > 0  ORDER BY `register_payment_status`.`id_register_payment_status` ASC');
            $this->data['reload1'] = $this->modelo_universal->query('SELECT * FROM `register_payment`, `register_payment_status`, `user` WHERE `register_payment_status`.`id_register_payment_status`= 1 and `register_payment_status`.`id_register_payment_status`=`register_payment`.`register_payment_status_id` and `register_payment`.`id_user`= `user`.`id_user`  AND `register_payment`.`amount` < 0  ORDER BY `register_payment_status`.`id_register_payment_status` ASC');
            $this->data['reload'] = $reload;
            $this->header('admin');
            $this->navigation();
            $this->load->view('index-admin', $this->data);
        }
    }

    public function last_hour() {
        date_default_timezone_set("America/Caracas");
        $hora = date('Y-m-d H:i:s', time() - 3600 * date('I'));
        return $hora;
    }

    public function activity() {
        $role = parent::verify_role();
        if ($role == true) {
//            $this->load->view('page/header');
//            $activity_status = $this->modelo_universal->select('activity_bet', '*', null);
            $this->data['activity'] = $this->modelo_universal->query('SELECT `activity_bet`.*,`user`.`nickname` FROM `activity_bet`,`user` WHERE `activity_bet`.`id_user` = `user`.`id_user` ORDER BY `id_activity_bet` DESC');
//            $this->data['activity'] = $activity_status;
//            debug($this->data);
            $this->header('admin');
            $this->navigation();
            $this->load->view('page/activity');
        }
    }

    public function status_payments() {
        $role = parent::verify_role();
        if ($role == true) {
//            $this->load->view('page/header');
           // $where = "register_payment_status.id_register_payment_status=register_payment.register_payment_status_id";

             $jackpot= $this->modelo_universal->query('SELECT  ROUND(SUM(`debt`), 2) as jackpot FROM `casino_jackpot`');
            //debug($jackpot[0]['jackpot']);
            $this->data['jackpots']=$jackpot[0]['jackpot'];
            $recent_payments = $this->modelo_universal->count('register_payment', 'register_payment_status_id = 1');
            //debug($recent_payments);
            $this->data['recent_payments'] = $recent_payments;
            $active_users = $this->modelo_universal->count('user', 'id_user_account_status = 3');
            //debug($recent_payments);
            $this->data['active_users'] = $active_users;

            $reload = $this->modelo_universal->query('SELECT * FROM `register_payment`, `register_payment_status`, `user` WHERE `register_payment_status`.`id_register_payment_status`=`register_payment`.`register_payment_status_id` and `register_payment`.`id_user`= `user`.`id_user` AND `register_payment`.`amount` > 0  ORDER BY `register_payment_status`.`id_register_payment_status`  ASC');
            $this->data['reload1'] = $this->modelo_universal->query('SELECT * FROM `register_payment`, `register_payment_status`, `user` WHERE `register_payment_status`.`id_register_payment_status`=`register_payment`.`register_payment_status_id` and `register_payment`.`id_user`= `user`.`id_user` AND `register_payment`.`amount` < 0  ORDER BY `register_payment_status`.`id_register_payment_status`  ASC');
            $this->data['reload'] = $reload;

            $this->header('admin');
            $this->navigation();
            $this->load->view('page/status_payments', $this->data);
        }
    }
    public function withdrawals() {
        $role = parent::verify_role();
        if ($role == true) {
//            $this->load->view('page/header');
           // $where = "register_payment_status.id_register_payment_status=register_payment.register_payment_status_id";

             $jackpot= $this->modelo_universal->query('SELECT  ROUND(SUM(`debt`), 2) as jackpot FROM `casino_jackpot`');
            //debug($jackpot[0]['jackpot']);
            $this->data['jackpots']=$jackpot[0]['jackpot'];
            $recent_payments = $this->modelo_universal->count('register_payment', 'register_payment_status_id = 1');
            //debug($recent_payments);
            $this->data['recent_payments'] = $recent_payments;
            $active_users = $this->modelo_universal->count('user', 'id_user_account_status = 3');
            //debug($recent_payments);
            $this->data['active_users'] = $active_users;

            $reload = $this->modelo_universal->query('SELECT * FROM `register_payment`, `register_payment_status`, `user` WHERE `register_payment_status`.`id_register_payment_status`=`register_payment`.`register_payment_status_id` and `register_payment`.`id_user`= `user`.`id_user` AND `register_payment`.`amount` < 0  ORDER BY `register_payment_status`.`id_register_payment_status`  ASC');
            $this->data['reload'] = $reload;

            $this->header('admin');
            $this->navigation();
            $this->load->view('page/status_payments1', $this->data);
        }
    }

    public function profile($message = null) {
        $role = parent::verify_role();
        if ($role == true) {

             $jackpot= $this->modelo_universal->query('SELECT  ROUND(SUM(`debt`), 2) as jackpot FROM `casino_jackpot`');
            //debug($jackpot[0]['jackpot']);
            $this->data['jackpots']=$jackpot[0]['jackpot'];
            $recent_payments = $this->modelo_universal->count('register_payment', 'register_payment_status_id = 1');
            //debug($recent_payments);
            $this->data['recent_payments'] = $recent_payments;
            $active_users = $this->modelo_universal->count('user', 'id_user_account_status = 3');
            //debug($recent_payments);
            $this->data['active_users'] = $active_users;

            if ($message == 'online') {
                $users = $this->modelo_universal->query('SELECT * FROM `user`, `user_account_status`, `active_session`  where `user`.`id_user_status` =1 and `user`.`id_user_account_status`= `user_account_status`.`id_user_account_status` and `user`.`id_user`= `active_session`.`id_user` and TIMESTAMPDIFF(MINUTE,`active_session`.`date_time`,NOW())< 60');
                $this->data['message'] = $message;
                $this->data['users'] = $users;
                $this->load->view('page/header');
                $this->navigation();
                $this->load->view('page/profile', $this->data);
            } elseif ($message == 'offline') {
                $users = $this->modelo_universal->query('SELECT * FROM `user`, `user_account_status`, `active_session`  where `user`.`id_user_status` =1 and `user`.`id_user_account_status`= `user_account_status`.`id_user_account_status` and `user`.`id_user`= `active_session`.`id_user` and TIMESTAMPDIFF(MINUTE,`active_session`.`date_time`,NOW())> 60');
                $this->data['message'] = $message;
                $this->data['users'] = $users;
                $this->load->view('page/header');
                $this->navigation();
                $this->load->view('page/profile', $this->data);
            } else {
                $users = $this->modelo_universal->query('SELECT * FROM `user` , `user_account_status` where `user`.`id_user_account_status`= `user_account_status`.`id_user_account_status`');
//            $this->index();
                $this->data['message'] = 'Todos';
                $this->data['users'] = $users;
                $this->load->view('page/header');
                $this->navigation();
                $this->load->view('page/profile', $this->data);
            }
        }
    }

    public function detail_profile($id = null) {
        $role = parent::verify_role();
        if ($role == true) {

            if (!$id) {
                redirect('./casino/profile');
            }
            if (isset($_POST['register_payment'])) {

                //Aqui Actualizar
            } else {

                $user = $this->modelo_universal->query('SELECT `user_data`.* ,`user`.* , `user_account_status`.`name` FROM `user_data`,`user`, `user_account_status` where `user_data`.id_user=' . $id . ' AND `user`.id_user=' . $id . ' AND `user`.`id_user_account_status` = `user_account_status`.`id_user_account_status`');

                $bet = $this->modelo_universal->query('SELECT * FROM `activity_bet` where id_user=' . $id);

                $balance = $this->modelo_universal->query('SELECT * FROM `activity_balance` where id_user=' . $id);

                $game = $this->modelo_universal->query('SELECT * FROM `game` where id_user=' . $id);

                $where = "register_payment_status.id_register_payment_status=register_payment.register_payment_status_id AND amount>0 AND register_payment.id_user = " . $id;

                $reload = $this->modelo_universal->selectjoin('register_payment', 'register_payment_status', $where, '*');
                
                $where1 = "register_payment_status.id_register_payment_status=register_payment.register_payment_status_id AND amount<0 AND register_payment.id_user = " . $id;

                $reload1 = $this->modelo_universal->selectjoin('register_payment', 'register_payment_status', $where1, '*');


                //$this->data['data'] = $data;
                $this->data['user'] = $user;
                $this->data['bet'] = $bet;
                $this->data['balance'] = $balance;
                $this->data['game'] = $game;
                $this->data['reload'] = $reload;
                $this->data['reload1'] = $reload1;
                $this->load->view('page/header');
                $this->navigation();
                $this->load->view('page/detail_profile', $this->data);
            }
        }
    }
    public function balance_detail(){ 

        $role = parent::verify_role();
        if ($role == true) {
//            $this->load->view('page/header');
//            $activity_status = $this->modelo_universal->select('activity_bet', '*', null);
            $this->data['balance_casino'] = $this->modelo_universal->select('casino_jackpot', '*');
            $this->data['sumapy'] = $this->modelo_universal->select('register_payment', 'SUM(amount)',array('register_payment_status_id'=>2)); 
            $this->data['sumacoin'] = $this->modelo_universal->select('user_data', 'SUM(coins)');
     
//            $this->data['activity'] = $activity_status;
//            debug($this->data);
            $this->header('admin');
            $this->navigation();
            $this->load->view('page/balance_detail', $this->data);
        }

    }
    public function update_payment($id = null) {
        $role = parent::verify_role();
        if ($role == true) {
            if (!isset($_POST["update_payment"])) {

                /* if(!$id && !isset($_POST['update_payment'])){
                  redirect('./casino/profile');
                  } */

                $payment = $this->modelo_universal->query('SELECT * FROM `register_payment` where id_register_payment=' . $id);
                $payment_status = $this->modelo_universal->select('register_payment_status', '*');


                $this->data['status'] = $payment_status;
                $this->data['payment'] = $payment;
//            debug($this->data,false);
                //debug(print_r($this->data));
                $this->load->view('page/header');
                $this->navigation();
                $this->load->view('page/update_payment', $this->data);
            } else {
//            debug($_POST);
                $id = $_POST['id_register_payment'];
                //echo "entro en el else del post";
                //debug($this->input->post('register_payment_status_id'));
                $p = $this->modelo_universal->select('register_payment_status', 'id_register_payment_status', array('name' => $_POST["register_payment_status_id"]));
//            debug($p[0]["id_register_payment_status"],false);
                $r = $this->modelo_universal->update('register_payment', array('register_payment_status_id' => $p[0]["id_register_payment_status"]), array('id_register_payment' => $id));
//            UPDATE  `v1`.`register_payment` SET  `register_payment_status_id` =  '1' WHERE  `register_payment`.`id_register_payment` =1;
//            debug($r);
                if (($r == 1) && ($p[0]["id_register_payment_status"] == 2 && (int) $_POST["amount"] > 0)) {
//                debug($p[0]["id_register_payment_status"]);
//                ["amount"]
                    $pa = $this->modelo_universal->select('user_data', 'coins', array('id_user' => $_POST["id_user"]));
//                    debug($pa);
                    $amount = (int) $pa[0]["coins"] + (int) $_POST["amount"];
//                    debug($amount);
                    $r1 = $this->modelo_universal->update('user_data', array('coins' => $amount), array('id_user' => $_POST["id_user"]));
                    if ($r1 == 1) {
                        $this->data['mensaje'] = "Estatus del Pago Actualizado";
                    }
                }
                  elseif (($r == 1) && ($p[0]["id_register_payment_status"] == 3 && (int) $_POST["amount"] < 0)) {
//                debug($p[0]["id_register_payment_status"]);
//                ["amount"]
                    $pa = $this->modelo_universal->select('user_data', 'coins', array('id_user' => $_POST["id_user"]));
//                    debug($pa);
                    $amount = (int) $pa[0]["coins"] + ((int) $_POST["amount"] * -1 );
//                    debug($amount);
                    $r1 = $this->modelo_universal->update('user_data', array('coins' => $amount), array('id_user' => $_POST["id_user"]));
                    if ($r1 == 1) {
                        $this->data['mensaje'] = "Estatus del Pago Actualizado";
                    }
                }
//            debug('otro');
                $payment = $this->modelo_universal->query('SELECT * FROM `register_payment` where id_register_payment=' . $_POST["id_register_payment"]);
                $payment_status = $this->modelo_universal->select('register_payment_status', '*');
                $this->data['status'] = $payment_status;
                $this->data['payment'] = $payment;

                $this->navigation();
                $this->load->view('page/header');
                $this->load->view('page/update_payment', $this->data);
            }
        } else {
//            debug('no roll');
            $this->close();
        }
    }

    public function login() {
        parent::login();
    }

    public function close() {
        parent::close();
    }

    public function close_home() {
        parent::close_home();
    }

    public function pr() {
        $insert = $this->modelo_universal->query('SELECT * FROM `user`');
        debug($insert);


        debug('');

        $this->sign_verify();
        $this->load->view('page/index');
    }

    public function watch_game() {
        $role = parent::verify_role();
        if ($role == true) {
            $this->header('admin');
            $this->navigation();
            $this->load->view('page/watch-game');
        }
    }

    public function load_debt($id) {

        $role = parent::verify_role();
        if ($role == true && $this->session->userdata('moneywt')) {

            if($this->input->post()){
                $this->modelo_universal->insert('register_debt',array('id_jackpot'=>$id,'bank'=>$this->input->post('bank'),'nume_ref'=>$this->input->post('bank'),'amount'=>$this->input->post('cuenta'),'bank'=>$this->input->post('bank'),'type'=>$this->input->post('type'),'account'=>$this->input->post('account'),'id_user'=>$this->session->userdata('id_user')));
                 $data = $this->modelo_universal->select('casino_jackpot', '*', array('id_jackpot' =>$id));
                          if($data){
                            $pote=$data[0]['debt']-$this->input->post('amount');
                            $this->modelo_universal->update('casino_jackpot',array('debt'=>$pote),array('id_jackpot' =>$id));

                            redirect('./casino');


        }

            }
            else{
        $data = $this->modelo_universal->select('casino_jackpot', '*', array('id_jackpot' =>$id));
        if($data){

            $this->data =$data[0];
                  $this->header('admin');
            $this->navigation();
         $this->load->view('page/load_debt');
        }
        }
        }
       
    }
        public function slotmachine() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 1');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 1');
        $data = array('consulta' => $result);

        $this->load->view('slotmachine/CSSettings', $data);
        
        $this->load->view('slotmachine/index');
    }

    public function slotmachine_marino() {
        $this->last_connection();

        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 2');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 2');
        $data = array('consulta' => $result);
        $this->load->view('slot_marino/CSSettings', $data);
        $this->load->view('slot_marino/index');
    }

    public function slotmachine_espacial() {
        $this->last_connection();

        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 3');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 3');
        $data = array('consulta' => $result);
        $this->load->view('slot_espacial/CSSettings', $data);
        $this->load->view('slot_espacial/index');
    }

    public function slotmachine_egipcia() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 4');
        $this->load->view('slot_egipcia/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 4');
        $data = array('consulta' => $result);

        $this->load->view('slot_egipcia/CSSettings', $data);
        $this->load->view('slot_egipcia/index');
    }

     public function slotmachine_ranas() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 5');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 5');
        $data = array('consulta' => $result);

        $this->load->view('slot_ranas/CSSettings', $data);
        $this->load->view('slot_ranas/index');
    }
     public function slotmachine_deportivo() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 5');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 5');
        $data = array('consulta' => $result);
        $this->load->view('slot_deportivo/CSSettings', $data);
        $this->load->view('slot_deportivo/index');
    }
    
     public function slotmachine_bebidas() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 7');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 7');
        $data = array('consulta' => $result);
        $this->load->view('slot_bebidas/CSSettings', $data);
        $this->load->view('slot_bebidas/index');
    }
     public function slotmachine_candy() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 8');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 8');
        $data = array('consulta' => $result);
        $this->load->view('slot_candy/CSSettings', $data);
        $this->load->view('slot_candy/index');
    }
     public function slotmachine_4as() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 9');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 9');
        $data = array('consulta' => $result);
        $this->load->view('slot_4as/CSSettings', $data);
        $this->load->view('slot_4as/index');
    }
    public function slotmachine_sensual() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 10');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 10');
        $data = array('consulta' => $result);
        $this->load->view('slot_sensual/CSSettings', $data);
        $this->load->view('slot_sensual/index');
    }
     public function slotmachine_musical() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 11');
        $this->load->view('slot_musical/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 11');
        $data = array('consulta' => $result);
        $this->load->view('slot_musical/CSSettings', $data);
        $this->load->view('slot_musical/index');
    }
    public function slotmachine_diamantes() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_slotmachine', '*', 'id_game_slotmachine = 11');
        $this->load->view('slotmachine/settings', $data[0]);
        $result = $this->modelo_universal->select('symbol_win_ocurrence', '*', 'id_game_slot = 11');
        $data = array('consulta' => $result);
        $this->load->view('slot_diamantes/CSSettings', $data);
        $this->load->view('slot_diamantes/index');
    }
    public function demo_slotmachine() {
        $this->load->view('slotmachine/demo-index');
    }
    public function demo_egipcia() {
        $this->load->view('slot-egipcia/demo-index');
    }
    public function roulette() {
        $data = $this->modelo_universal->select('game_roulette', '*', 'id_game_roulette = 1');
        $this->load->view('roulette/settings', $data[0]);
        $this->load->view('roulette/index');
    }

    public function blackjack() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_blackjack', '*', 'id_game_blackjack = 1');
        $this->load->view('blackjack/settings', $data[0]);
        $this->load->view('blackjack/index');
    }

    public function jacks() {
        $this->last_connection();
        $data = $this->modelo_universal->select('game_jacksorbetter', '*', 'id_game_jacksorbetter = 1');
        $this->load->view('jacks/settings', $data[0]);
        $this->load->view('jacks/index');
    }


    public function pdf(){
            $this->last_connection();
                //debug($_POST);
                $query = $_POST['query'];
                $f = $this->data['activity'] = $this->modelo_universal->query($query);
                //$html = "";
                //$html = "<table>";
                $html = $this->load->view('page/pdf',$this->data['activity'],true);


                //debug($html);
                //$this->load->view('page/pdf',$this->data);
          
                $this->load->library('Pdf');
                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

                    //debug(PDF_HEADER_LOGO_WIDTH);
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('Israel Parra');
                $pdf->SetTitle('Ejemplo de provincías con TCPDF');
                $pdf->SetSubject('Tutorial TCPDF');
                $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
         
        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
                $pdf->SetHeaderData('logo.png', 20, 'Casino4As', '', array(0, 64, 255), array(0, 64, 128));
                $pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));
         
        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
         
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
                $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
         
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        //relación utilizada para ajustar la conversión de los píxeles
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         
         
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
                $pdf->setFontSubsetting(true);
         
        // Establecer el tipo de letra
         
        //Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
                $pdf->SetFont('freemono', '', 10, '', true);
         
        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
                $pdf->AddPage();
         
        //fijar efecto de sombra en el texto
                $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
                    
                    //html=$this->load->view('page/pdf',true);
                $html = $this->load->view('page/pdf',$this->data['activity'],true);

                $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
         
        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
                $nombre_archivo = utf8_decode("Localidades de .pdf");
                $pdf->Output($nombre_archivo, 'I');
                
            
    }


    public function reportes(){
            $this->last_connection();
        $role = parent::verify_role();
        if ($role == true) {
           
            if(isset($_POST['buscar'])){
                
                if(($_POST['hasta'] != '') || ($_POST['desde'] != '') || ($_POST['nombre'] != '')){
                    $_POST['nombre'];
                    $_POST['desde'];
                    $_POST['hasta'];
                    if(($_POST['hasta'] == '') && ($_POST['desde'] == '')){
                        $query = "SELECT `activity_bet`.*,`user`.`nickname` FROM `activity_bet`,`user` WHERE `activity_bet`.`id_user` = `user`.`id_user` AND `user`.`nickname` LIKE '%".$_POST['nombre']."%' ORDER BY `id_activity_bet` DESC";
                    }else{
                        if($_POST['hasta'] == ''){
                            $hoy = date('Y-m-d');
                        }else{
                            $hoy = $_POST['hasta'];
                        }

                        if($_POST['desde'] == ''){
                            $query = "SELECT `activity_bet`.*,`user`.`nickname` FROM `activity_bet`,`user` WHERE `activity_bet`.`id_user` = `user`.`id_user` AND (`user`.`nickname` LIKE '%".$_POST['nombre']."%' AND `activity_bet`.`time_f` < '2016-02-11') ORDER BY `id_activity_bet` DESC";
                        }else{
                            $query = "SELECT `activity_bet`.*,`user`.`nickname` FROM `activity_bet`,`user` WHERE `activity_bet`.`id_user` = `user`.`id_user` AND (`user`.`nickname` LIKE '%".$_POST['nombre']."%' AND `activity_bet`.`time_f` < '".$hoy."' AND `activity_bet`.`time_f` > '".$_POST['desde']."') ORDER BY `id_activity_bet` DESC";
                        }
                    }
                }else{
                    $query = 'SELECT `activity_bet`.*,`user`.`nickname` FROM `activity_bet`,`user` WHERE `activity_bet`.`id_user` = `user`.`id_user` ORDER BY `id_activity_bet` DESC';
                }
                $this->data['query'] = $query;

                $d = $this->data['activity'] = $this->modelo_universal->query($query);
                //debug($d);
                $this->session->set_flashdata('lastquery',$this->db->last_query());
                $this->header('admin');
                $this->navigation();
                $this->load->view('page/reportes');
            }else{
                $this->header('admin');
                $this->navigation();
                $this->load->view('page/reportes1');
            }

        }else{

        }
    }

    public function promotion(){
            $this->last_connection();
        $role = parent::verify_role();
        if ($role == true) {
           
            if(isset($_POST['crear'])){
                $this->modelo_universal->insert('promocion',array('nombre'=>$this->input->post('nombre'),'fecha_ini'=>$this->input->post('fecha_ini'),'fecha_fin'=>$this->input->post('fecha_fin'),'porcentaje'=>$this->input->post('porcentaje')));
                    redirect('promotion');
            }

                $this->data['promotion']=$this->modelo_universal->select('promocion','*');
                $this->header('admin');
                $this->navigation();
                $this->load->view('page/promotion');
            

        }
    }

    public function promotion_delete($id=null){
        if($id==null){
           redirect('promotion');
        }
            $this->last_connection();
        
        $role = parent::verify_role();
        if ($role == true) {
            $this->modelo_universal->delete('promocion',array('id'=>$id));
             redirect('promotion');

        }
    }




}
