<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Player extends MY_Controller {

    public $data = null;
    public $usuario = null;

    public function __construct() {
        parent::__construct();
        $this->load->model('modelo_universal');
        $this->load->helper('cookie');
        $this->load->library('session');
    }

    public function index() {
//        debug($_COOKIE);
        if (($this->input->cookie('token', true) != false)) {
        $this->token_cokie();
        }
        $role = parent::verify_role();
        if($role == false){
        $this->data['connect'] = $this->last_connection();

        //debug(print_r($this->session->userdata));
        if ($this->session->userdata('id_role') == false) {
            parent::index();
            //redirect('./completareg');
            //$this->modelo_universal->check('user_data', $this->session->userdata('id_user'));           
        }elseif ($this->session->userdata('id_role') == 1) {
            redirect('./dashboard');
        }else{
            $this->load->view('page/header2');
            $this->header('player');
//            $this->load->view('page/header');
//            debug($this->session->userdata('id_user'));
            $coins = $this->modelo_universal->select('user_data', 'coins,coinsfree,first_name,last_name', array('id_user'=>$this->session->userdata('id_user')));
            $this->data['coins'] = $coins[0]['coins'];
               $this->data['coinsfree'] = $coins[0]['coinsfree'];
            $this->data['first_name'] = $coins[0]['first_name'];
            $this->data['last_name'] = $coins[0]['last_name'];
//            debug($coins);
            $this->navigation();
            $this->load->view('page/index', $this->data);
        }
    }
    }
    
   
    
    public function registering(){
        $role = parent::verify_role();
        if($role == false){
        $this->load->view('page/registering');
        
//        $n = $this->input->post('namenick');
//        $e = $this->input->post('email');
//        $p = md5($this->input->post('password'));
//        $insert = $this->modelo_universal->check('user', array('nickname' => 'pkadmin','email' => 'jfigueroapcs@gmail.com','pass' => $p,'id_role'=> 2),null,null, true);
//        debug($insert);
//        if($insert != null){
//            $this->validar_post($n, $this->input->post('password'));
//        }
        }
    }

        public function user_profile() {
            $this->last_connection();
            $role = parent::verify_role();
            if($role == false){
                //debug(print_r($this->session->userdata('id_user')));
                //$data = $this->modelo_universal->select('user_data', '*', array('id_user' =>  56));
                $data = $this->modelo_universal->select('user_data', '*', array('id_user' =>  $this->session->userdata('id_user')));
                //debug(print_r($data));
                //debug($data);
            

               /* if (!$data){
                    redirect('./inser_controller/insertc');
                }*/
                $this->data['dat'] = $data;
                $this->data['term'] = true;
            //debug(print_r($this->session->userdata));
      
                $this->header('player');
                $this->navigation(); 
                $this->load->view('page/insert/registercompl');
    
            }
        }




        public function payments() {
            $this->last_connection();
            $role = parent::verify_role();
            if($role == false){
                //debug(print_r($this->session->userdata('id_user')));
                $where="register_payment_status.id_register_payment_status=register_payment.register_payment_status_id AND register_payment.id_user = ".$this->session->userdata('id_user')." AND `amount` > 0";
                $data = $this->modelo_universal->selectjoin('register_payment','register_payment_status',$where,'*' );

                
                $this->data['data'] = $data;
                //debug(print_r($data[0]));
                 $this->load->view('page/header2');
                $this->header('player');
                $this->navigation();
                $this->load->view('player/payments');
                $this->load->view('page/footer2');
            }
        }
        public function removing_balance() {
            $this->last_connection();
            $role = parent::verify_role();
            if($role == false){

            $coins = $this->modelo_universal->select('user_data', 'coins,first_name,last_name,bank,numberbank', array('id_user'=>$this->session->userdata('id_user')));
            $numberbank=$coins[0]['numberbank'];
            $bank=$coins[0]['bank'];
            $coins = $coins[0]['coins'];
                
                if (isset($_POST['monto'])){



                if($_POST['monto'] <= $coins && $_POST['monto']>0 ){
                    
                    //debug($_POST['monto']);
                    $amount = $this->input->post('monto')*-1;
                    $newamount = $coins - $this->input->post('monto');
                    //debug($newamount);
                        $data = array(
                            'nume_ref' => $numberbank,
                            'type' => 'Transferencia',
                            'bank' => $bank,
                            'amount' => $amount,
                            'id_user' => $this->session->userdata('id_user'),
                            'register_payment_status_id'=>'1',
                            'register_date' => date("Y-m-d")
                            );
                        //debug($data);
                        $this->modelo_universal->insert('register_payment', $data);

                        $r1 = $this->modelo_universal->update('user_data', array('coins' => $newamount), array('id_user' => $this->session->userdata('id_user')));
                        if ($r1 == 1) {
                        $this->data['mensaje'] = "Estatus del Pago Actualizado";
                        $this->session->set_flashdata('mensaje',"Estatus del Pago Actualizado");
                        redirect('./removing-balance');
                        //debug($this->data['mensaje']);
                    }

                    
                }else{
                    //debug("error");
                 
                   
                    $this->session->set_flashdata('mensaje',"No se pudo realizar la oeperaci&oacute;n");
                       redirect('./removing-balance');
                }
            

               }


                //debug(print_r($this->session->userdata('id_user')));
                $where="register_payment_status.id_register_payment_status=register_payment.register_payment_status_id AND register_payment.id_user = ".$this->session->userdata('id_user')." AND `amount` < 0";
                $data = $this->modelo_universal->selectjoin('register_payment','register_payment_status',$where,'*' );
                $this->data['data'] = $data;
                $this->data['coins'] = $coins;
                $this->data['bank'] = $bank;
                $this->data['numberbank'] = $numberbank;
                //debug(print_r($data[0]));
                 $this->load->view('page/header2');
                $this->header('player');
                $this->navigation();
                $this->load->view('player/payments1');
                $this->load->view('page/footer2');
            }
        }
        

        public function load_payment() {
            $this->last_connection();
            $role = parent::verify_role();
            if($role == false){
               if (isset($_POST['register_payment'])) {
                    $this->form_validation->set_rules('nume_ref', 'N° Referencia', 'required|trim|xss_clean|min_length[4]|max_length[10]|is_unique[register_payment.nume_ref]');
                    $this->form_validation->set_rules('type', 'Tipo De Instrumento', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('bank', 'Banco', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('amount', 'Monto', 'required|xss_clean');

                    $this->form_validation->set_message('required', 'El %s es requerido');
                    $this->form_validation->set_message('is_unique', 'Este %s ya se encuentra en la base de datos');
                    

                    if ($this->form_validation->run() == FALSE) {
                        $this->data['id_user'] = $this->session->userdata('id_user');
                        $this->session->set_flashdata('message', 'Revisar Datos de Formulario');
                        $this->header('player');
                        $this->navigation();
                        $this->load->view('player/load_payment');
                    } else {
                         //echo "validations true";

                            if($this->input->post('id_user') != $this->session->userdata('id_user')){
                                 //echo "session error";
                                //$data = null;
                                $this->session->set_flashdata('message', 'Error en la sesion, Inicie sesion nuevamente');
                                //$this->header('player');
                                //$this->navigation();
                               // $this->load->view('player/load_payment');
                                redirect('player/load_payment');
                            }else{
                              
                            
                        $data = array(
                            'nume_ref' => $this->input->post('nume_ref'),
                            'type' => $this->input->post('type'),
                            'bank' => $this->input->post('bank'),
                            'amount' => $this->input->post('amount'),
                            'id_user' => $this->session->userdata('id_user'),
                            'register_payment_status_id'=>'1',
                            'register_date' => date("Y-m-d")
                            );

                        $this->modelo_universal->insert('register_payment', $data);
                          $now=date("Y-m-d");
                       
     $promocion= $this->modelo_universal->select('promocion','*',array('fecha_ini <='=> $now,'fecha_fin >='=> $now));
                              if($promocion){
                             $data = array(
                            'nume_ref' => $this->input->post('nume_ref')." promoción ".$promocion[0]['porcentaje']."%",
                            'type' => $this->input->post('type'),
                            'bank' => $promocion[0]['nombre']." ".$promocion[0]['porcentaje']."%",
                            'amount' => ($this->input->post('amount')*$promocion[0]['porcentaje'])/100,
                            'id_user' => $this->session->userdata('id_user'),
                            'register_payment_status_id'=>'1',
                            'register_date' => date("Y-m-d")
                            );
                               $this->modelo_universal->insert('register_payment', $data);
                        }
                               $this->load->library('email');
   $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'tls://p3plcpnl0553.prod.phx3.secureserver.net',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@casino4as.com.ve',
            'smtp_pass' => 'Pr4y2ct4',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );         
        $this->email->initialize($config);


        $this->email->to("yoescarlay.rodriguez@casino4as.com");
        $this->email->subject('Nuevo pago');
       
        $this->email->message("Monto: ".$this->input->post('amount'));
                        //$data = null;;
                       // $this->header('player');
                       // $this->navigation();
                        $this->data['id_user'] = $this->session->userdata('id_user');
                        $this->session->set_flashdata('message', 'Su pago fue registrado exitosamente y se encuentra en espera de aprobación');
                       // echo "<script>alert('Su pago fue registrado exitosamente y se encuentra en espera de aprobación');</script>";
                        //$this->load->view('player/load_payment', $this->data);
                        redirect('player/load_payment');
                        }                    
                    }



                    //debug(print_r($this->data['id_user']));            
                    
                    
                }else{
                     //echo "entrando";
                    $data = null;
                    $this->data['id_user'] = $this->session->userdata('id_user');
                    $this->load->view('page/header2');
                    $this->header('player');
                    $this->data['promocion'] = $this->modelo_universal->select('promocion','*',array('fecha_ini <='=> date("Y-m-d"),'fecha_fin >='=>date("Y-m-d")));
                    $this->session->set_flashdata('message', null);
                    $this->navigation();
                    $this->load->view('player/load_payment', $this->data);
                    $this->load->view('page/footer2');
                    //redirect('player/load_payment');
                }
            }
        }

}
