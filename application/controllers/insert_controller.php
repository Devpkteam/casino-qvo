<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Insert_controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('modelo_universal');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('cookie');
    }

    public function newest() {

        $this->load->view('page/login');
    }

    function receivingData() {
        $this->modelo_universal->delete('user_session', array('user_token' => $this->input->cookie('token', true)));
        $this->session->unset_userdata('session');
        $this->session->unset_userdata('session_id');
        $this->session->unset_userdata('ip_address');
        $this->session->unset_userdata('user_agent');
        $this->session->unset_userdata('id_role');
        $this->session->unset_userdata('last_activity');
        delete_cookie('wordpress');
        delete_cookie('token');

        if (isset($_POST['id_user_account_status']) and $_POST['id_user_account_status'] == '0') {
            //SI EXISTE EL CAMPO OCULTO LLAMADO GRABAR CREAMOS LAS VALIDACIONES
            //$this->form_validation->set_rules('nickname','Nombre','required|trim|xss_clean');
            $this->form_validation->set_rules('email', 'Correo', 'valid_email|required|trim|xss_clean|is_unique[user.email]');
            $this->form_validation->set_rules('nickname', 'Seudónimo', 'required|trim|xss_clean|min_length[5]|max_length[12]|is_unique[user.nickname]');
            $this->form_validation->set_rules('pass', 'Clave', 'matches[passc]|md5');
            $this->form_validation->set_rules('passc', 'Password Confirmation', 'required');

            //SI HAY ALGÚNA REGLA DE LAS ANTERIORES QUE NO SE CUMPLE MOSTRAMOS EL MENSAJE
            //EL COMODÍN %s SUSTITUYE LOS NOMBRES QUE LE HEMOS DADO ANTERIORMENTE, EJEMPLO, 
            //SI EL NOMBRE ESTÁ VACÍO NOS DIRÍA, EL NOMBRE ES REQUERIDO, EL COMODÍN %s 
            //SERÁ SUSTITUIDO POR EL NOMBRE DEL CAMPO
            $this->form_validation->set_message('required', 'El %s es requerido');
            $this->form_validation->set_message('valid_email', 'El %s no es válido');
            $this->form_validation->set_message('is_unique', 'Este %s ya se encuentra registrado');
            $this->form_validation->set_message('matches', 'La  %s no coincide con la confirmación');

            //SI ALGO NO HA IDO BIEN NOS DEVOLVERÁ AL INDEX MOSTRANDO LOS ERRORES
            if ($this->form_validation->run() == FALSE) {
                $this->newest();
            } else {

                $correo = $this->input->post('email');
                $nick = $this->input->post('nickname');

                $data = array(
                    'nickname' => $this->input->post('nickname'),
                    'email' => $this->input->post('email'),
                    'pass' => $this->input->post('pass'),
                    'id_user_account_status' => '0',
                    'id_role'=>'2',
                    'cod_validacion'=>md5($this->input->post('nickname'))

                    );
                $this->enviarcorreo($correo , $nick);
                

                $check= $this->modelo_universal->query('SELECT nickname FROM user WHERE nickname = "'.$this->input->post('nickname').'" OR email = "'.$this->input->post('email').'"');
                 if(!$check){
                $this->modelo_universal->insert('user', $data);
               
                $this->insertado();
                }
                else{
                    $this->session->set_flashdata('message2','El usuario o email ya existe');
                      $this->newest();
                }
            }
        }
    }

    public function recibirDc() {
//        debug($_POST,false);


        if (isset($_POST['id_user_account_status'])) {
            //SI EXISTE EL CAMPO OCULTO LLAMADO GRABAR CREAMOS LAS VALIDACIONES
            //$this->form_validation->set_rules('nickname','Nombre','required|trim|xss_clean');
            $this->form_validation->set_rules('first_name', 'Nombre', 'required|xss_clean|trim(last_name)');
            $this->form_validation->set_rules('last_name', 'Apellido', 'required|xss_clean|trim(last_name)');
            $this->form_validation->set_rules('identity_card', 'Nº de Identificación', 'required|trim|xss_clean|is_unique[user_data.identity_card]');
            $this->form_validation->set_rules('gender', 'Género', 'required|trim|xss_clean');
            $this->form_validation->set_rules('date_of_birth', 'Fecha de Nacimiento', 'required|trim|xss_clean');
            $this->form_validation->set_rules('phone', 'Teléfono', 'required|trim|xss_clean');
            $this->form_validation->set_rules('nationality', 'Nacionalidad', 'required|trim|xss_clean');
            $this->form_validation->set_rules('country', 'Pais', 'required|trim|xss_clean');
            $this->form_validation->set_rules('city', 'Ciudad', 'required|trim|xss_clean');
            $this->form_validation->set_rules('address', 'Dirección', 'required|trim|xss_clean');
            $this->form_validation->set_rules('n_cuenta', 'Numero de cuenta', 'required|trim|xss_clean');
            $this->form_validation->set_rules('banco', 'Banco', 'required|trim|xss_clean');
            //SI HAY ALGÚNA REGLA DE LAS ANTERIORES QUE NO SE CUMPLE MOSTRAMOS EL MENSAJE
            //EL COMODÍN %s SUSTITUYE LOS NOMBRES QUE LE HEMOS DADO ANTERIORMENTE, EJEMPLO, 
            //SI EL NOMBRE ESTÁ VACÍO NOS DIRÍA, EL NOMBRE ES REQUERIDO, EL COMODÍN %s 
            //SERÁ SUSTITUIDO POR EL NOMBRE DEL CAMPO
            $this->form_validation->set_message('required', 'El %s es requerido');

            $this->form_validation->set_message('is_unique', 'Este %s ya se encuentra registrado');


            //SI ALGO NO HA IDO BIEN NOS DEVOLVERÁ AL INDEX MOSTRANDO LOS ERRORES


         /*   if ($this->form_validation->run() == FALSE) {
                $this->insertc();
               } else {*/

               // $this->load->view('upload_success', $imagen);
            
            $very = $this->modelo_universal->select('user_data', '*', array('id_user' => $this->session->userdata('id_user')));
            
            
//debug($very);
            
            if(!$very){
                $data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name' => $this->input->post('lastname'),
                    'identity_card' => $this->input->post('identity_card'),
                    'gender' => $this->input->post('gender'),
                    'date_of_birth' => $this->input->post('date_of_birth'),
                    'phone' => $this->input->post('phone'),
                    'nationality' => $this->input->post('nationality'),
                    'country' => $this->input->post('country'),
                    'city' => $this->input->post('city'),
                    'address' => $this->input->post('address'),
                    'id_user' => $this->session->userdata('id_user'),
                    'bank' => $this->input->post('banco'),
                    'numberbank' => $this->input->post('n_cuenta')
                    );
                $this->modelo_universal->insert('user_data', $data);
                  $title= 'Documento Identificación';
                $id_user= $this->session->userdata('id_user');

                $this->save($title,$id_user);

            //actualizar status
        $data2 = array(
            'id_user_account_status' => '3'
            );

        $this->modelo_universal->update('user', $data2, array('id_user' => $id_user));

                 $this->session->set_flashdata('mensaje','Tus datos se han actualizado correctamente');
                 // debug($this->session->userdata('name'),false);
                 // debug($this->session->userdata('pass'));
               parent::validar_post($this->session->userdata('name'),$this->session->userdata('pass'),null,true);
               exit();
           // }
            
            }else{
                
                     $data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name' => $this->input->post('lastname'),
//                    'identity_card' => $this->input->post('identity_card'),
                    'gender' => $this->input->post('gender'),
                    'date_of_birth' => $this->input->post('date_of_birth'),
                    'phone' => $this->input->post('phone'),
                    'nationality' => $this->input->post('nationality'),
                    'country' => $this->input->post('country'),
                    'city' => $this->input->post('city'),
                    'address' => $this->input->post('address'),
                    'bank' => $this->input->post('banco'),
                    'numberbank' => $this->input->post('n_cuenta')

//                    'id_user' => $this->session->userdata('id_user')
                    );
                    

        if($this->input->post('password',true) && $this->input->post('password',true) == $this->input->post('password2',true) ){
                $this->modelo_universal->update('user', array('pass'=>md5($this->input->post('password',true))), array('id_user' => $this->session->userdata('id_user')));
            
        }
                $this->modelo_universal->update('user_data', $data, array('id_user' => $this->session->userdata('id_user')));
//                debug($this->session->userdata('id_user'));
                    $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
        $config['max_size'] = '2000';
        $config['max_width']  = '2000';
        $config['max_height']  = '2000';
        $config['encrypt_name']  = true;
        
        
        
        $this->load->library('upload', $config);
    
        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
        
        }else{  
            
            $data = $this->upload->data();
            
            //$data2="http://casino4as.com/casino/images/".$data['file_name'];
            $data2 = base_url().'images/'.$data['file_name'];
                $this->modelo_universal->update('user_data', array('imagedi'=>$data2), array('id_user' => $this->session->userdata('id_user')));
        }
        
        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '4000';
        $config['max_width']  = '4000';
        $config['max_height']  = '4000';
        $config['encrypt_name']  = true;
        
        
        
        $this->load->library('upload', $config);
    
        if ( ! $this->upload->do_upload('userfile2'))
        {
            $error = array('error' => $this->upload->display_errors());
        
        }else{  
            
            $data =  $this->upload->data();
            //$data2="http://casino4as.com/casino/images/".$data['file_name'];
            $data2 = base_url().'images/'.$data['file_name'];
                $this->modelo_universal->update('user_data', array('imageprofile'=>$data2), array('id_user' => $this->session->userdata('id_user')));
        }
        
        

                $this->session->set_flashdata('mensaje','Tus datos se han actualizado correctamente');
                redirect('./myprofile');
            }

              
        }
    }

    public function insertado() {
        // $this->load->view('page/header');
        // $this->header('player');
        // $this->load->view('page/insert/insertado');
        
        // parent::index();
        parent::header('pl/ayer');
        $this->load->view('page/insert/insertado');
    }
    public function prp(){

        // parent::index();
        parent::header('pl/ayer');
        $this->load->view('page/insert/insertado');
    }

    public function insertc() {

        // parent::index();
        parent::header('pl/ayer');
        $this->load->view('page/insert/registercompl', array('data' => ' ' ));
    }

    public function enable($id = null) {

        if(!$id)
        {
            redirect('./newest');
        }


        $data = array(
            'id_user_account_status' => '1'
            );
        $check=$this->modelo_universal->select('user','email',array('cod_validacion'=>$id,'id_user_account_status <>'=>1));
        if($check){
            $this->modelo_universal->update('user', $data, array('cod_validacion' => $id));
   $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'tls://p3plcpnl0553.prod.phx3.secureserver.net',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@casino4as.com.ve',
            'smtp_pass' => 'Pr4y2ct4',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );         
        $this->email->initialize($config);


            $this->email->from('noreply@casino4as.com.ve', 'Casino4As.com');
        $this->email->to($check[0]['email']);
        $this->email->subject('Compre fichas Casino4As.com');

        $html=$this->load->view('page/sendmsgpay',null,true);
        $this->email->message($html);

            // Tu nombre de usuario es: ' . $nick . '.

        $this->email->send();
        }
        
//        $check = $this->modelo_universal->select('user', '*', array('cod_validacion' => $id));
//        if(empty($check)){
//        }else{
//            $this->session->set_userdata(array('token' => $this->session->userdata('session_id')));
//                    $token = $this->session->userdata('token');
//
//                    $s = $this->modelo_universal->select('active_session', '*', array('id_user' => $check[0]['id_user']));
//
//                    if ($s == null) {
//                        $date = $this->last_hour();
//                        $this->modelo_universal->insert('active_session', array('token' => $token, 'id_user' => $check[0]['id_user'], 'date_time' => $date));
//                    } else {
//                        $this->last_connection();
//                    }
//                    $this->session->set_userdata(array('session' => md5('true')));
//                    $this->session->set_userdata(array('name' => $check[0]['nickname']));
//                    $this->session->set_userdata(array('token' => $token));
//                    $this->session->set_userdata(array('id_role' => $check[0]['id_role']));
//                    $this->session->set_userdata(array('id_user' => $check[0]['id_user']));
//                    if($this->session->userdata('id_role') == 1){
//                        redirect('./casino');
//                    }else{
//
//
//                        redirect('./account');
//                        
//                    }
//        }
        // $this->load->view('page/header');
        
   
        // parent::index();
        parent::header('pl/ayer');
        $this->load->view('page/insert/enable');

    }

    function enviarcorreo($correo , $nick)
    {
   $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'tls://p3plcpnl0553.prod.phx3.secureserver.net',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@casino4as.com.ve',
            'smtp_pass' => 'Pr4y2ct4',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );         
        $this->email->initialize($config);





          $this->email->from('noreply@casino4as.com.ve', 'Casino4As.com');
        $this->email->to($correo);
        $this->email->subject('Bienvenido/a a Casino4As.com');
        $this->data['nick']=$nick;
        $html=$this->load->view('page/sendmsg',$this->data,true);
        $this->email->message($html);

            // Tu nombre de usuario es: ' . $nick . '.

        $this->email->send();
   $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'tls://p3plcpnl0553.prod.phx3.secureserver.net',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@casino4as.com.ve',
            'smtp_pass' => 'Pr4y2ct4',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );         
        $this->email->initialize($config);
        $this->email->from('noreply@casino4as.com.ve', 'Casino4As.com');
        $this->email->to("marketing@casino4as.com.ve");
        $this->email->subject('Nuevo registro');
       
        $this->email->message("NICK: ".$nick." CORREO:".$correo);

            // Tu nombre de usuario es: ' . $nick . '.

        $this->email->send();


    }


public function save($title, $id_user)
    {
        $this->load->model('save_img');

        $url = $this->do_upload();
        
        $this->save_img->save($title, $url, $id_user);
    }
    private function do_upload()
    {
        $type = explode('.', $_FILES["userfile"]["name"]);
        $type = strtolower($type[count($type)-1]);
        $url = "./images/".uniqid(rand()).'.'.$type;
        if(in_array($type, array("jpg", "jpeg", "gif", "png")))
            if(is_uploaded_file($_FILES["userfile"]["tmp_name"]))
                if(move_uploaded_file($_FILES["userfile"]["tmp_name"],$url))
                    return $url;
        return "";
    }


 /*   function 


     $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload())
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('page/insert/registercompl', $error);
                }
                else
                {
                        $imagen = array('upload_data' => $this->upload->data());*/


}
