<?php //debug($coins); ?>

<div id="page-wrapper" class="custom-login-panel">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Retirar Saldo</h1> 
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default " style="background: transparent !important;">
                        <!-- /.panel-heading -->
                        <div class="panel-body custom-panel-body">

                        <!--<form action="http://casino4as.com/casino/receivingdc" method="post" accept-charset="utf-8" enctype="multipart/form-data">-->
                        <?php echo form_open_multipart("/removing-balance") ?>                        
                        <fieldset>
                                          <div class=" col-lg-12 col-xs-12 col-sm-12 col-md-4 sin-padding">
                                    <div class="form-group input-group">
                                     <span class="input-group-addon"><span class="glyphicon glyphicon-piggy-bank"></span></span>
                                    <input class="form-control disabled" readonly="" placeholder="LLene el Banco en mi perfil" name="" type="text" required="" minlength="20" maxlength="20" value="<?php
                                    if (isset($bank)) {
                                        echo $bank;
                                    } ?>" /> 

                                </div>       
                             
                             </div>
                                <div class=" col-lg-12 col-xs-12 col-sm-12 col-md-4 sin-padding">
                                    <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input class="form-control disable" readonly="" placeholder="N° de Cuenta" name="" type="text" id="" required="" maxlength="20" value="<?php
                                    if (isset($numberbank)) {
                                        echo $numberbank;
                                    }?>">
                                     <input class="form-control hidden"   placeholder="N° de Cuenta" name="" type="text" id="" required="" maxlength="20" value="<?php
                                    if (isset($numberbank)) {
                                        echo $numberbank;
                                    }?>"> 

                                </div>       
                             
                             </div>
                             <div class="col-xs-12 col-sm-8 col-sm-offset-2 sin-padding">
                                <div class="form-group input-group sin-padding">
                                    <span class="input-group-addon"><span class="glyphicon"><img class="glyphicon" style="width: 20px;height: 20px;" src="./imagen/poker/ficha-5.png" /></span></span>
                                    <input type="number" class="form-control" name="monto"  placeholder="Monto" value="<?=$coins?>" required="" max="<?=$coins?>" min="0" title="Maximo de puntos disponibles <?=$coins?>"/>

                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"></font>

                                </div>
                            </div>

                            
                            <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                <p style="color:white;">Por favor adjunte su imagen de perfil.</p>
                                <div class="form-group input-group">
                                    
                                    <span class="input-group-addon"><span class="fa fa-photo" aria-hidden="true"></span></span>

                                    <input class="form-control" placeholder="Imagen" name="userfile2" type="file" > 


                                </div>
                            </div>-->
                                                        <input class="form-control" placeholder="" name="registration_date" type="hidden" value="0000-00-00" required=""> 
                            <!-- Change this to a button or input when using this as a form -->
                            <!--<a href="index.html" class="btn btn-lg btn-success btn-block">iniciar sesi&oacute;n</a>-->
                            <div class="clearfix"></div>
                            <input type="submit" name="login" class="btn btn-lg btn-success btn-block" value="Guardar Cambios">

                        </fieldset>
                        </form>



                        </div>
                        </div>

                        <div class="col-lg-12">
                    <div class="panel panel-default ">



                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-user">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Tipo de Instrumento</th>
                                            <th>Banco</th>
                                            <th>N° referencia</th>
                                            <th>Monto</th>
                                            <th>Fecha de Registro</th>
                                            <th>Estatus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($this->data['data'] as $recarga=>$data ){ ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $data['id_register_payment']; ?></td>
                                            <td><?php echo $data['type']; ?></td>
                                            <td><?php echo $data['bank']; ?></td>
                                            <td><?php echo $data['nume_ref']; ?></td>
                                            <td><?php echo $data['amount']; ?></td>
                                            <td><?php echo $data['register_date']; ?></td>
                                            <td><?php echo $data['name']; ?></td>
                                        </tr>
                                        
                                        <?php } ?>
                                    </tbody>
                                </table>
                                
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <?php if($this->session->flashdata('mensaje')){ ?>
        <script>
        alert('<?= $this->session->flashdata('mensaje')?>');
        </script>

        <?php } ?>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->


</body>

</html>
