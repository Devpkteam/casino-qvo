<?php //debug($dat[0]['imageprofile']); ?>

    <?php $this->load->view('page/header2'); 
                                if(isset($dat)){
                                        
                                }
                                ?>
<div id="page-wrapper" class="custom-login-panel" style="margin:0px !important;">

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">

                <div class="login-panel panel panel-default custom-login-panel">
                    <div class="panel-heading custom-panel-heading">
                        <h3 class="panel-title custom-panel-title"><?php
                            if ($this->session->flashdata('mensaje') != false) {
                                echo $this->session->flashdata('mensaje');
                            } elseif (isset($dat)) {
                                echo 'Datos Personales ';
                            } else {
                                ?>Complete su Registro<?php } ?></h3>
                    </div>
                    <div class="panel-body custom-panel-body">
                        <?php echo form_open_multipart("/receivingdc") ?>
                        <!--     <form role="form" method="post" action="./registering"> -->
                        <fieldset>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 sin-padding col-lg-offset-4 col-sm-offset-3 col-xs-offset-0">
                                <?php 
                                if(isset($dat) && isset($dat[0]['imageprofile']) &&  $dat[0]['imageprofile'] != null){
                                    $img = $dat[0]['imageprofile'];
                                }else{
                                    $img = 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRj_r_Dpw-apPm9rF34wOiTwAdyR8lkVYEmvEGS6VBBUU7iQAC7Ag';
                                }
                                
                                 ?>
                                <div class="img-profile" id="img-c" style="background: url(<?=$img;?>)no-repeat;background-position: center;background-size: cover;height:200px !important">
                                    <!--<img src="./interface/images/recortes/home/logo.png" alt="">-->
                                </div>      
                                
                                
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                    <p style="color:white;">Por favor adjunte su imagen de perfil.</p>
                                    <div class="form-group input-group">
                                        
                                        <span class="input-group-addon"><span class="fa fa-photo" aria-hidden="true"></span></span>
    
                                        <input class="form-control" placeholder="Imagen" name="userfile2" type="file"> 
    
    
                                    </div>
                                </div>
                                
                            </div>
                            <div class="clearfix"></div>
                             <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-users"></span></span>
                                    <select style="" class="form-control select-country" name="nationality" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['nationality'];
                                    } else
                                        echo set_value('nationality')
                                        ?>">
                                        <option value="">Nacionalidad</option>
                                        <option <?php
                                        if (isset($dat) && $dat[0]['nationality'] == 'V') {
                                            echo 'selected';
                                        }
                                        ?> value="V">Venezolano</option>
                                        <option <?php
                                        if (isset($dat) && $dat[0]['gender'] == 'E') {
                                            echo 'selected';
                                        }
                                        ?> value="E">Extranjero</option>

                                    </select>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-eye-slash"></span></span>

                                    <input class="form-control" name="id_user" type="hidden" value="<?php
                                    if (isset($data)) {
                                        echo $data;
                                    }
                                    ?>" required=""> 
                                    <input class="form-control" name="id_user_account_status" type="hidden" value="2" required=""> 
                                    <input class="form-control" placeholder="N° de Identificación" name="identity_card" type="text" required value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['identity_card'];
                                    } else
                                        echo set_value('identity_card')
                                        ?>"required="" <?php if (isset($dat[0]['identity_card'])) { echo 'readonly="readonly"';} ?>> 
                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('identity_card'); ?></font>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group sin-padding">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input type="hidden" name="id_user_account_status" value="0" />
                                    <input type="text" class="form-control" name="firstname" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['first_name'];
                                    } else
                                        echo set_value('firstname')
                                        ?>" placeholder="Nombre" required="" pattern=".{3,20}"title="5 a 12 caracteres">

                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('nickname'); ?></font>

                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input type="text" class="form-control" name="lastname" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['last_name'];
                                    } else
                                        echo set_value('lastname')
                                        ?>" placeholder="Apellido" required="" pattern=".{3,20}"title="5 a 12 caracteres">
                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('email'); ?></font>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input class="form-control" placeholder="Fecha de Nacimiento" name="date_of_birth" max="<?php $fecha = date('Y-m-d');
$nuevafecha = strtotime ( '-18 year' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'Y-m-d' , $nuevafecha ); echo $nuevafecha; $nuevafecha2 = strtotime ( '-110 year' , strtotime ( $fecha ) ) ;$nuevafecha2 = date ( 'Y-m-d' , $nuevafecha2 ); ?>" min="<?php echo $nuevafecha2; ?>" type="date" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['date_of_birth'];
                                    } else
                                        echo set_value('date_of_birth')
                                        ?>"required=""> 
                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('date_of_birth'); ?></font>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-users"></span></span>
                                    <select  class="form-control select-country" name="gender" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['gender'];
                                    } else
                                        echo set_value('gender')
                                        ?>">
                                        <option value="">Sexo</option>
                                        <option <?php
                                        if (isset($dat) && $dat[0]['gender'] == 'M') {
                                            echo 'selected';
                                        }
                                        ?> value="M">Masculino</option>
                                        <option <?php
                                        if (isset($dat) && $dat[0]['gender'] == 'F') {
                                            echo 'selected';
                                        }
                                        ?> value="F">Femenino</option>

                                    </select>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-phone" ></span></span>

                                    <input class="form-control" placeholder="N° de Teléfono" name="phone" type="number" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['phone'];
                                    } else
                                        echo set_value('phone')
                                        ?>"required=""> 
                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('phone'); ?></font>

                                </div>
                            </div>
                     
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
                                    
                                    <!--<input class="form-control" placeholder="País" name="country" type="text" value="<?php
                                    //if (isset($dat)) {-->
                                     // echo $dat[0]['country'];-->
                                    //} else-->
                                    //  echo set_value('country')-->
                                    ?>"required=""> -->
                                    <select name="country" class="form-control select-country" id="" >
                                       <?php if (isset($dat)) { ?> <option><?php
                                      echo $dat[0]['country'];
                                   
                                    ?></option><?php } ?>
                                        <option value="Afganistán">Afganistán</option>
                                        <option value="Albania">Albania</option>
                                        <option value="Alemania">Alemania</option>
                                        <option value="Andorra">Andorra</option>
                                        <option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option>
                                        <option value="Antártida">Antártida</option>
                                        <option value="Antigua y Barbuda">Antigua y Barbuda</option>
                                        <option value="Antillas Holandesas">Antillas Holandesas</option>
                                        <option value="Arabia Saudí">Arabia Saudí</option>
                                        <option value="Argelia">Argelia</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Armenia">Armenia</option>
                                        <option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Austria">Austria</option>
                                        <option value="Azerbaiyán">Azerbaiyán</option>
                                        <option value="Bahamas">Bahamas</option>
                                        <option value="Bahrein">Bahrein</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option>
                                        <option value="Bélgica">Bélgica</option>
                                        <option value="Belice">Belice</option>
                                        <option value="Benin">Benin</option>
                                        <option value="Bermudas">Bermudas</option>
                                        <option value="Bielorrusia">Bielorrusia</option>
                                        <option value="Birmania">Birmania</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Bosnia y Herzegovina">Bosnia y Herzegovina</option>
                                        <option value="Botswana">Botswana</option>
                                        <option value="Brasil">Brasil</option>
                                        <option value="Brunei">Brunei</option>
                                        <option value="Bulgaria">Bulgaria</option>
                                        <option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option>
                                        <option value="Bután">Bután</option>
                                        <option value="Cabo Verde">Cabo Verde</option>
                                        <option value="Camboya">Camboya</option>
                                        <option value="Camerún">Camerún</option>
                                        <option value="Canadá">Canadá</option>
                                        <option value="Chad">Chad</option>
                                        <option value="Chile">Chile</option>
                                        <option value="China">China</option>
                                        <option value="Chipre">Chipre</option>
                                        <option value="Ciudad del Vaticano (Santa Sede)">Ciudad del Vaticano (Santa Sede)</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="Comores">Comores</option>
                                        <option value="Congo">Congo</option>
                                        <option value="Congo, República Democrática del">Congo, República Democrática del</option>
                                        <option value="Corea">Corea</option>
                                        <option value="Corea del Norte">Corea del Norte</option>
                                        <option value="Costa de Marfíl">Costa de Marfíl</option>
                                        <option value="Costa Rica">Costa Rica</option>
                                        <option value="Croacia (Hrvatska)">Croacia (Hrvatska)</option>
                                        <option value="Cuba">Cuba</option>
                                        <option value="Dinamarca">Dinamarca</option>
                                        <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Egipto">Egipto</option>
                                        <option value="El Salvador">El Salvador</option>
                                        <option value="Emiratos Árabes Unidos">Emiratos Árabes Unidos</option>
                                        <option value="Eritrea">Eritrea</option>
                                        <option value="Eslovenia">Eslovenia</option>
                                        <option value="España">España</option>
                                        <option value="Estados Unidos">Estados Unidos</option>
                                        <option value="Estonia">Estonia</option>
                                        <option value="Etiopía">Etiopía</option>
                                        <option value="Fiji">Fiji</option>
                                        <option value="Filipinas">Filipinas</option>
                                        <option value="Finlandia">Finlandia</option>
                                        <option value="Francia">Francia</option>
                                        <option value="Gabón">Gabón</option>
                                        <option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option>
                                        <option value="Granada">Granada</option>
                                        <option value="Grecia">Grecia</option>
                                        <option value="Groenlandia">Groenlandia</option>
                                        <option value="Guadalupe">Guadalupe</option>
                                        <option value="Guam">Guam</option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guayana">Guayana</option>
                                        <option value="Guayana Francesa">Guayana Francesa</option>
                                        <option value="Guinea">Guinea</option>
                                        <option value="Guinea Ecuatorial">Guinea Ecuatorial</option>
                                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                                        <option value="Haití">Haití</option>
                                        <option value="Honduras">Honduras</option>
                                        <option value="Hungría">Hungría</option>
                                        <option value="India">India</option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Irak">Irak</option>
                                        <option value="Irán">Irán</option>
                                        <option value="Irlanda">Irlanda</option>
                                        <option value="Isla Bouvet">Isla Bouvet</option>
                                        <option value="Isla de Christmas">Isla de Christmas</option>
                                        <option value="Islandia">Islandia</option>
                                        <option value="Islas Caimán">Islas Caimán</option>
                                        <option value="Islas Cook">Islas Cook</option>
                                        <option value="Islas de Cocos o Keeling">Islas de Cocos o Keeling</option>
                                        <option value="Islas Faroe">Islas Faroe</option>
                                        <option value="Islas Heard y McDonald">Islas Heard y McDonald</option>
                                        <option value="Islas Malvinas">Islas Malvinas</option>
                                        <option value="Islas Marianas del Norte">Islas Marianas del Norte</option>
                                        <option value="Islas Marshall">Islas Marshall</option>
                                        <option value="Islas menores de Estados Unidos">Islas menores de Estados Unidos</option>
                                        <option value="Islas Palau">Islas Palau</option>
                                        <option value="Islas Salomón">Islas Salomón</option>
                                        <option value="Islas Svalbard y Jan Mayen">Islas Svalbard y Jan Mayen</option>
                                        <option value="Islas Tokelau">Islas Tokelau</option>
                                        <option value="Islas Turks y Caicos">Islas Turks y Caicos</option>
                                        <option value="Islas Vírgenes (EEUU)">Islas Vírgenes (EEUU)</option>
                                        <option value="Islas Vírgenes (Reino Unido)">Islas Vírgenes (Reino Unido)</option>
                                        <option value="Islas Wallis y Futuna">Islas Wallis y Futuna</option>
                                        <option value="Israel">Israel</option>
                                        <option value="Italia">Italia</option>
                                        <option value="Jamaica">Jamaica</option>
                                        <option value="Japón">Japón</option>
                                        <option value="Jordania">Jordania</option>
                                        <option value="Kazajistán">Kazajistán</option>
                                        <option value="Kenia">Kenia</option>
                                        <option value="Kirguizistán">Kirguizistán</option>
                                        <option value="Kiribati">Kiribati</option>
                                        <option value="Kuwait">Kuwait</option>
                                        <option value="Laos">Laos</option>
                                        <option value="Lesotho">Lesotho</option>
                                        <option value="Letonia">Letonia</option>
                                        <option value="Líbano">Líbano</option>
                                        <option value="Liberia">Liberia</option>
                                        <option value="Libia">Libia</option>
                                        <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lituania">Lituania</option>
                                        <option value="Luxemburgo">Luxemburgo</option>
                                        <option value="Macedonia, Ex-República Yugoslava de">Macedonia, Ex-República Yugoslava de</option>
                                        <option value="Madagascar">Madagascar</option>
                                        <option value="Malasia">Malasia</option>
                                        <option value="Malawi">Malawi</option>
                                        <option value="Maldivas">Maldivas</option>
                                        <option value="Malí">Malí</option>
                                        <option value="Malta">Malta</option>
                                        <option value="Marruecos">Marruecos</option>
                                        <option value="Martinica">Martinica</option>
                                        <option value="Mauricio">Mauricio</option>
                                        <option value="Mauritania">Mauritania</option>
                                        <option value="Mayotte">Mayotte</option>
                                        <option value="México">México</option>
                                        <option value="Micronesia">Micronesia</option>
                                        <option value="Moldavia">Moldavia</option>
                                        <option value="Mónaco">Mónaco</option>
                                        <option value="Mongolia">Mongolia</option>
                                        <option value="Montserrat">Montserrat</option>
                                        <option value="Mozambique">Mozambique</option>
                                        <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Nicaragua">Nicaragua</option>
                                        <option value="Níger">Níger</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Niue">Niue</option>
                                        <option value="Norfolk">Norfolk</option>
                                        <option value="Noruega">Noruega</option>
                                        <option value="Nueva Caledonia">Nueva Caledonia</option>
                                        <option value="Nueva Zelanda">Nueva Zelanda</option>
                                        <option value="Omán">Omán</option>
                                        <option value="Países Bajos">Países Bajos</option>
                                        <option value="Panamá">Panamá</option>
                                        <option value="Papúa Nueva Guinea">Papúa Nueva Guinea</option>
                                        <option value="Paquistán">Paquistán</option>
                                        <option value="Paraguay">Paraguay</option>
                                        <option value="Perú">Perú</option>
                                        <option value="Pitcairn">Pitcairn</option>
                                        <option value="Polinesia Francesa">Polinesia Francesa</option>
                                        <option value="Polonia">Polonia</option>
                                        <option value="Portugal">Portugal</option>
                                        <option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option>
                                        <option value="Reino Unido">Reino Unido</option>
                                        <option value="República Centroafricana">República Centroafricana</option>
                                        <option value="República Checa">República Checa</option>
                                        <option value="República de Sudáfrica">República de Sudáfrica</option>
                                        <option value="República Dominicana">República Dominicana</option>
                                        <option value="República Eslovaca">República Eslovaca</option>
                                        <option value="Reunión">Reunión</option>
                                        <option value="Ruanda">Ruanda</option>
                                        <option value="Rumania">Rumania</option>
                                        <option value="Rusia">Rusia</option>
                                        <option value="Sahara Occidental">Sahara Occidental</option>
                                        <option value="Saint Kitts y Nevis">Saint Kitts y Nevis</option>
                                        <option value="Samoa">Samoa</option>
                                        <option value="Samoa Americana">Samoa Americana</option>
                                        <option value="San Marino">San Marino</option>
                                        <option value="San Vicente y Granadinas">San Vicente y Granadinas</option>
                                        <option value="Santa Helena">Santa Helena</option>
                                        <option value="Santa Lucía">Santa Lucía</option>
                                        <option value="Santo Tomé y Príncipe">Santo Tomé y Príncipe</option>
                                        <option value="Senegal">Senegal</option>
                                        <option value="Seychelles">Seychelles</option>
                                        <option value="Sierra Leona">Sierra Leona</option>
                                        <option value="Singapur">Singapur</option>
                                        <option value="Siria">Siria</option>
                                        <option value="Somalia">Somalia</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="St Pierre y Miquelon">St Pierre y Miquelon</option>
                                        <option value="Suazilandia">Suazilandia</option>
                                        <option value="Sudán">Sudán</option>
                                        <option value="Suecia">Suecia</option>
                                        <option value="Suiza">Suiza</option>
                                        <option value="Surinam">Surinam</option>
                                        <option value="Tailandia">Tailandia</option>
                                        <option value="Taiwán">Taiwán</option>
                                        <option value="Tanzania">Tanzania</option>
                                        <option value="Tayikistán">Tayikistán</option>
                                        <option value="Territorios franceses del Sur">Territorios franceses del Sur</option>
                                        <option value="Timor Oriental">Timor Oriental</option>
                                        <option value="Togo">Togo</option>
                                        <option value="Tonga">Tonga</option>
                                        <option value="Trinidad y Tobago">Trinidad y Tobago</option>
                                        <option value="Túnez">Túnez</option>
                                        <option value="Turkmenistán">Turkmenistán</option>
                                        <option value="Turquía">Turquía</option>
                                        <option value="Tuvalu">Tuvalu</option>
                                        <option value="Ucrania">Ucrania</option>
                                        <option value="Uganda">Uganda</option>
                                        <option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistán">Uzbekistán</option>
                                        <option value="Vanuatu">Vanuatu</option>
                                        <option value="Venezuela" <?php if (!isset($dat)) { ?>selected <?php } ?>>Venezuela</option>
                                        <option value="Vietnam">Vietnam</option>
                                        <option value="Yemen">Yemen</option>
                                        <option value="Yugoslavia">Yugoslavia</option>
                                        <option value="Zambia">Zambia</option>
                                        <option value="Zimbabue">Zimbabue</option>
                                    </select>
                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('country'); ?></font>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sin-padding">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-location-arrow"></span></span>

                                    <input class="form-control" placeholder="Ciudad" name="city" type="text" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['city'];
                                    } else
                                        echo set_value('city')
                                        ?>"required=""> 
                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('city'); ?></font>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">

                                <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-location-arrow" aria-hidden="true"></span></span>

                                    <input class="form-control" placeholder="Dirección" name="address" type="address" value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['address'];
                                    } else
                                        echo set_value('address')
                                        ?>"required=""> 
                                    <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('address'); ?></font>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                <p style="color:white;">Por favor adjunte su documento de identidad escaneado. (Soló son permitidos los formatos gif | jpg | png | jpeg | pdf, y un peso maximo de 2mb o 1000px de ancho y alto)</p>
                                <div class="form-group input-group">
                                    
                                    <span class="input-group-addon"><span class="fa fa-photo" aria-hidden="true"></span></span>

                                    <input class="form-control" placeholder="Imagen" name="userfile" type="file" > 


                                </div>
                            </div>

                             <div class=" col-lg-6 col-xs-12 col-sm-12 col-md-4 sin-padding">
                             <div class="form-group input-group">
                             <span class="input-group-addon"><span class="glyphicon glyphicon-piggy-bank"></span></span>
                                     <select name="banco" class="form-control select-country" required="">
                                                       <?php if (isset($dat) && $dat[0]['bank'] != "") { ?> <option><?php
                                      echo $dat[0]['bank'];
                                   
                                    ?></option><?php }else{
                                        ?>
                                        <option value="">Seleccione su banco</option>
                                        <?

                                    } ?>
                                             
                                              <option>100%BANCO</option>
                                              <option>ABN AMRO BANK</option>
                                              <option>BANESCO, C.A.</option>
                                              <option>BANCAMIGA BANCO MICROFINANCIERO, C.A.</option>
                                              <option>BANCO ACTIVO BANCO COMERCIAL, C.A.</option>
                                              <option>BANCO AGRICOLA</option>
                                              <option>BANCO BICENTENARIO</option>
                                              <option>BANCO CARONI, C.A. BANCO UNIVERSAL</option>
                                              <option>BANCO DE DESARROLLO DEL MICROEMPRESARIO</option>
                                              <option>BANCO DE VENEZUELA S.A.I.C.A.</option>
                                              <option>BANCO DEL CARIBE C.A.</option>
                                              <option>BANCO DEL PUEBLO SOBERANO C.A.</option>
                                              <option>BANCO DEL TESORO</option>
                                              <option>BANCO ESPIRITO SANTO, S.A.</option>
                                              <option>BANCO EXTERIOR C.A.</option>
                                              <option>BANCO INDUSTRIAL DE VENEZUELA.</option>
                                              <option>BANCO INTERNACIONAL DE DESARROLLO, C.A.</option>
                                              <option>BANCO MERCANTIL C.A.</option>
                                              <option>BANCO NACIONAL DE CREDITO</option>
                                              <option>BANCO OCCIDENTAL DE DESCUENTO.</option>
                                              <option>BANCO PLAZA</option>
                                              <option>BANCO PROVINCIAL BBVA</option>
                                              <option>BANCO VENEZOLANO DE CREDITO S.A.</option>
                                              <option>BANCRECER S.A. BANCO DE DESARROLLO</option>
                                              <option>BANFANB</option>
                                              <option>BANGENTE</option>
                                              <option>BANPLUS BANCO COMERCIAL C.A</option>
                                              <option>CITIBANK.</option>
                                              <option>CORP BANCA.</option>
                                              <option>DELSUR BANCO UNIVERSAL</option>
                                              <option>FONDO COMUN</option>
                                              <option>INSTITUTO MUNICIPAL DE CR&#201;DITO POPULAR</option>
                                              <option>MIBANCO BANCO DE DESARROLLO, C.A.</option>
                                              <option>SOFITASA</option>
                                        </select>
                              </div>   
                              </div>
                               <div class=" col-lg-6 col-xs-12 col-sm-12 col-md-4 sin-padding">
                                    <div class="form-group input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input class="form-control" placeholder="N° de Cuenta" name="n_cuenta" pattern=".{0}|.{20}" type="text" id="money-text"  value="<?php
                                    if (isset($dat)) {
                                        echo $dat[0]['numberbank'];
                                    } else
                                        echo set_value('numberbank')
                                        ?>"> 

                                </div>       
                             
                             </div>
                            <?php if (isset($dat)) { ?>
                           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                <p style="color:white;">Si desea cambiar la clave llene los campos</p>
                                <div class="form-group input-group">

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                    <div class="form-group input-group sin-padding">
                                    <span class="input-group-addon"><span class="fa fa-lock" ></span></span></span><input class="form-control" placeholder="Nueva clave" name="password" type="password" >
                 
                                     <span class="input-group-addon"><span class="fa fa-lock" ></span></span></span><input class="form-control" placeholder="Repita su clave" name="password2" type="password" > 
                                </div>
                                </div>
                                </div>
                            </div>
                            <?php } ?>
                            <!--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                <p style="color:white;">Por favor adjunte su imagen de perfil.</p>
                                <div class="form-group input-group">
                                    
                                    <span class="input-group-addon"><span class="fa fa-photo" aria-hidden="true"></span></span>

                                    <input class="form-control" placeholder="Imagen" name="userfile2" type="file" > 


                                </div>
                            </div>-->
                            <?php if(!isset($term)){ ?>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                <p style="color:white;"><a href="http://casino4as.com/terminos-y-condiciones"target="_blank">Acepta ud los terminos y condiciones.</a></p>
                                <input type="checkbox" name="acepto" required/>
                            </div>
                            <?php } else{
?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sin-padding">
                                <p style="color:white;"><a href="http://casino4as.com/terminos-y-condiciones"target="_blank">Terminos y condiciones.</a></p>
                       
                            </div>
<?php

                                } ?>
                            <input class="form-control" placeholder="" name="registration_date" type="hidden" value="<?php
                            if (isset($dat)) {
                                echo $dat[0]['registration_date'];
                            } else
                                echo set_value('registration_date')
                                ?>"required=""> 
                            <!-- Change this to a button or input when using this as a form -->
                            <!--<a href="index.html" class="btn btn-lg btn-success btn-block">iniciar sesi&oacute;n</a>-->
                            <div class="clearfix"></div>
                            <input type="submit" name="login" class="btn btn-lg btn-success btn-block" value="<?php
                                   if (isset($dat)) {
                                       echo 'Guardar Cambios';
                                   } else {
                                       ?>Registrarme<?php } ?>"/>

                        </fieldset>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('page/footer2'); ?>
<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<style type="text/css">
    .img-profile {
    display: block;
    max-width: 200px;
    max-height: 200px;
    margin: 0 auto;
    clear: both;
    border-radius: 50%;
    overflow: hidden;
    border: 1px solid;
    height: 100%;
    width: 100%;
}


</style>
    <script>
    
    $(document).ready(function() {

         $('#money-text').keyup(function (event) {
                                      this.value = this.value.replace(/[^0-9\.]/g, '');
                                  });
    });
    </script>