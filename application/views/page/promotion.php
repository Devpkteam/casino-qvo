        <style type="text/css">
            #dataTables-example_filter,#dataTables-example_length{
                display: none !important; 
            }
        </style>
<?php //debug(); ?>
        <div id="page-wrapper"  class="custom-login-panel">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Promociones</h1> 
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row --> 
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
<!--                            DataTables Advanced Tables-->
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            <div class="">
                                <form role="form" method="post" action="./promotion">
                                   <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 sin-padding">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input class="form-control" placeholder="Desde" name="fecha_ini" max="<?=date('Y-m-d');?>" type="date" value="0000-00-00" > 
                                            <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"></font>

                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 sin-padding">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input class="form-control" placeholder="Hasta" name="fecha_fin" max="<?=date('Y-m-d');?>" type="date" value="0000-00-00" > 
                                            <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"></font>

                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 sin-padding">
                                        <div class="input-group">
                                          <input type="text" class="form-control" name="nombre" placeholder="Nombre">
                                          <span class="input-group-btn">
                                       
                                          </span>
                                          </div>
                                        </div><!-- /input-group -->
                                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 sin-padding">
                                        <div class="input-group">
                                          <input type="text" class="form-control" name="porcentaje" placeholder="Porcentaje">
                                          <span class="input-group-btn">
                                            <button class="btn btn-default" name="crear" type="submit">Crear</button>
                                          </span>
                                          </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            
                                            <th>Nombre</th>
                                            <th>Fecha in</th>
                                            <th>Fecha fin</th>
                                         
                                            <th>porcentaje</th>
                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php //debug(print_r($this->data['activity']));
                                            if(isset($this->data['promotion'])){
                                            foreach ($this->data['promotion'] as $p ){ ?>

                                        <tr class="odd gradeX">
                          <td><?php   echo $p['nombre']; ?></td>
                          <td><?php   echo $p['fecha_ini']; ?></td>
                          <td><?php   echo $p['fecha_fin']; ?></td>
                          <td><?php   echo $p['porcentaje']; ?></td>
                          <td><a href="./promotion_delete/<?php echo $p['id'] ?>">Eliminar</a></td>

                
                                        
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
