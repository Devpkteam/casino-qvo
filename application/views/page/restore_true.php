<!DOCTYPE html>
<html lang="es">

    <head>
        <base href="<?php echo base_url(); ?>" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Recuperar Contrase&ntilde;a - Casino4As</title>
        <link rel="stylesheet/less" type="text/css" href="css/main.less" />
         <link rel="stylesheet" type="text/less" href="/interface/css/main.less">
        <script src="js/less.min.js"></script>
    </head>
    
    <body>
        
        <div class="container-fluid" style="<width:100%></width:100%>">
        <?php $this->load->view('page/headerurls'); ?>
        
            <div class="row" style="margin-top:100px">
           
                <div class="clearfix"></div>
                
                
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default  custom-login-panel">
                    <?php if ($this->session->flashdata('message')!= null){
                        echo "<div id='infoMessage' class='alert alert-danger' role='alert'>". $this->session->flashdata('message') ."</div>";
                        }
                    ?>
              
                        <div class="panel-heading custom-panel-heading">
                            <h3 class="panel-title custom-panel-title">Recuperar Contrase&ntilde;a</h3>
                        </div>
                        <div class="panel-body custom-panel-body">
                                <label for="" style="color:white !important;">Se ha cambiado su contraseña con &eacute;xito. Para continuar, de clic e Inicie Sesi&oacute;n.<br><a href="./login">Iniciar Sesi&oacute;n</a></label>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
  <?php $this->load->view('page/footer2'); ?>

        <!-- jQuery -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>

    </body>

</html>
