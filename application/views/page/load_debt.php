 <div id="page-wrapper"  class="custom-login-panel">
            
     <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Registrar Retiro</h1> 
                
                <!-- /.col-lg-12 -->
            <div class="panel-body custom-panel-body">
                               <?php if ($this->session->flashdata('message')!= null){
                                    echo "<div id='infoMessage' class='alert alert-info' role='alert'>". $this->session->flashdata('message') ."</div>";
                                    }
                                ?>
                                <?php 
                                //debug($this->data);
                                ?>
                                 <form method="post">
                                <!--     <form role="form" method="post" action="./registering"> -->
                                <fieldset>
                                <input class="form-control" name="id_jackpot" id= 'id_jackpot' name= 'id_jackpot' type="hidden" readonly value="<?php if(isset($this->data)){ echo $this->data['id_jackpot']; }?>"> 
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                                     <!--    <input class="form-control" name="id_user" id= 'id_user' type="hidden" value="<?php if(isset($this->data)){ echo  $this->data['id_user']; }?>" required="">  -->
                                        <input class="form-control" placeholder="N° Referencia" name="nume_ref" id='nume_ref' type="text" pattern=".{4,10}" title="4 a 12 digitos"></font>
                                        <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('nume_ref'); ?></font>

                                    </div>

                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>

                                        <select class="form-control" name="type" id="type" >
                                            <option selected value="">Metodo de Retiro</option>
                                            <option value="Transferencia" style="color:black;">Transferencia</option>
                                            <option value="Deposito" style="color:black;">Retiro</option>
                                        </select>
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                                        <style>
                                            .form-control>option{
                                                color:black;
                                            }
                                        </style>
                                        <select name="banco" class="form-control">
                                              <option value="">Seleccione el banco</option>
                                              <option>100%BANCO</option>
                                              <option>ABN AMRO BANK</option>
                                              <option>BANESCO, C.A.</option>
                                              <option>BANCAMIGA BANCO MICROFINANCIERO, C.A.</option>
                                              <option>BANCO ACTIVO BANCO COMERCIAL, C.A.</option>
                                              <option>BANCO AGRICOLA</option>
                                              <option>BANCO BICENTENARIO</option>
                                              <option>BANCO CARONI, C.A. BANCO UNIVERSAL</option>
                                              <option>BANCO DE DESARROLLO DEL MICROEMPRESARIO</option>
                                              <option>BANCO DE VENEZUELA S.A.I.C.A.</option>
                                              <option>BANCO DEL CARIBE C.A.</option>
                                              <option>BANCO DEL PUEBLO SOBERANO C.A.</option>
                                              <option>BANCO DEL TESORO</option>
                                              <option>BANCO ESPIRITO SANTO, S.A.</option>
                                              <option>BANCO EXTERIOR C.A.</option>
                                              <option>BANCO INDUSTRIAL DE VENEZUELA.</option>
                                              <option>BANCO INTERNACIONAL DE DESARROLLO, C.A.</option>
                                              <option>BANCO MERCANTIL C.A.</option>
                                              <option>BANCO NACIONAL DE CREDITO</option>
                                              <option>BANCO OCCIDENTAL DE DESCUENTO.</option>
                                              <option>BANCO PLAZA</option>
                                              <option>BANCO PROVINCIAL BBVA</option>
                                              <option>BANCO VENEZOLANO DE CREDITO S.A.</option>
                                              <option>BANCRECER S.A. BANCO DE DESARROLLO</option>
                                              <option>BANFANB</option>
                                              <option>BANGENTE</option>
                                              <option>BANPLUS BANCO COMERCIAL C.A</option>
                                              <option>CITIBANK.</option>
                                              <option>CORP BANCA.</option>
                                              <option>DELSUR BANCO UNIVERSAL</option>
                                              <option>FONDO COMUN</option>
                                              <option>INSTITUTO MUNICIPAL DE CR&#201;DITO POPULAR</option>
                                              <option>MIBANCO BANCO DE DESARROLLO, C.A.</option>
                                              <option>SOFITASA</option>
                                            </select>
                                        <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('bank'); ?></font>
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                                        <input class="form-control" placeholder="Cuenta" name="cuenta" id ="cuenta" type="text" required="" maxlength="20"> 
                                        <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('cuenta'); ?></font>

                                    </div>       
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                                        <input class="form-control" placeholder="Monto" name="amount" id ="amount" type="text" required=""> 
                                        <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"><?php echo form_error('amount'); ?></font>

                                    </div>                            

                                      <!-- Change this to a button or input when using this as a form -->
                                    <!--<a href="index.html" class="btn btn-lg btn-success btn-block">iniciar sesi&oacute;n</a>-->

                                    <input type="submit" name="register_debt" class="btn btn-lg btn-success btn-block" value="Registrar"/>

                                </fieldset>
                               </form>
                            </div>
                            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
