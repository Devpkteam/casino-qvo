<footer>
    <div class="pre">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-12  col-xs-12 col-xs-offset-0 ">
                    <div class="redes ">
                        <a target="_blank" href="https://m.facebook.com/Casino4as-943407139075991/">
                            <img src="./interface/images/recortes/home/face.png" alt="">
                        </a>
                         <a target="_blank" href="https://twitter.com/casino4as casino4as.com/">
                            <img src="./interface/images/recortes/home/twit.png" alt="">
                        </a>
                        <a target="_blank" href="https://plus.google.com/100259890048187925393">
                            <img src="./interface/images/recortes/home/google.png" alt="">
                        </a>
                        <a target="_blank" href="https://www.instagram.com/casino4as/">
                            <img src="./interface/images/recortes/home/pinterest.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="logo-footer">
                        <img src="./interface/images/recortes/home/logo-footer.png" alt="">
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 hidden-xs hidden-sm">
                    <p class="copy">www.casino4s.com. Todos los derechos reservados<br>
                        Desarrollado por Proyecto Kamila</p>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <ul class="nav nav-pills">
                        <li role="presentation"><a href="http://www.casino4as.com?s=t" target="_blank">Inicio</a></li>
                        <li role="presentation"><a href="http://www.casino4as.com/nosotros?s=t" target="_blank">Nosotros</a></li>
                        <li role="presentation"><a href="http://www.casino4as.com/juego?s=t" target="_blank">Juegos</a></li>
                        <li role="presentation"><a href="http://www.casino4as.com/casino/poker" target="_blank">Póker</a></li>
                        <li role="presentation"><a href="http://www.casino4as.com/blog" target="_blank">Manual</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <!-- <p class="telefono hidden-xs"><span class="glyphicon glyphicon-earphone "></span> 900 234 80 32 <span class="glyphicon glyphicon-envelope email"></span> soporte@casino4as.com</p> -->
                    <div class="">
                        <!-- <p class="telefono"> <span class="glyphicon glyphicon-earphone "></span> 900 234 80 32 </p> -->
                        <p class="telefono"> <span class="glyphicon glyphicon-envelope email"></span> contacto@casino4as.com</p>
                    </div>
                </div>
                <div class=" col-xs-12 visible-xs visible-sm">
                    <p class="copy">www.casino4s.com. Todos los derechos reservados<br>
</p>
                </div>
            </div>
        </div>
    </div>
    </div>
</footer>