        <style type="text/css">
            #dataTables-example_filter,#dataTables-example_length{
                display: none !important; 
            }
        </style>
<?php //debug(); ?>
        <div id="page-wrapper"  class="custom-login-panel">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Actividades de apuestas</h1> 
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row --> 
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
<!--                            DataTables Advanced Tables-->
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                            <div class="">
                                <form role="form" method="post" action="./reportes">
                                   <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 sin-padding">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input class="form-control" placeholder="Desde" name="desde" max="<?=date('Y-m-d');?>" type="date" value="0000-00-00" > 
                                            <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"></font>

                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 sin-padding">
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input class="form-control" placeholder="Hasta" name="hasta" max="<?=date('Y-m-d');?>" type="date" value="0000-00-00" > 
                                            <font color="red" style="font-weight: bold; font-size: 8px; text-decoration: underline"></font>

                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 sin-padding">
                                        <div class="input-group">
                                          <input type="text" class="form-control" name="nombre" placeholder="Buscar Usuario...">
                                          <span class="input-group-btn">
                                            <button class="btn btn-default" name="buscar" type="submit">Buscar</button>
                                          </span>
                                          </div>
                                        </div><!-- /input-group -->
                                    </div>
                                </form>
                            </div>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th id="">Id Activity</th>
                                            <th>Juego</th>
                                            <th>Usuario</th>
                                            <th>Fecha hora Inicial</th>
                                            <th>Fecha hora Final</th>
                                            <th>Saldo Inicial</th>
                                            <th>Saldo Final</th>
                                            <th>minutos en juego</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php //debug(print_r($this->data['activity']));
                                            if(isset($this->data['activity'])){
                                            foreach ($this->data['activity'] as $activity=>$valor ){ ?>

                                        <tr class="odd gradeX">
                                            <td><?php if(isset($valor['id_activity_bet'])){ echo $valor['id_activity_bet'];} ?></td>
                                            <td><?php if(isset($valor['id_game'])){echo $valor['id_game'];} ?></td>
                                            <td><?php if(isset($valor['nickname'])){ echo $valor['nickname'];} ?></td>
                                            <td><?php if(isset($valor['time_i'])){ echo $valor['time_i'];} ?></td>
                                            <td><?php if(isset($valor['time_f'])){ echo $valor['time_f'];} ?></td>
                                            <td><?php if(isset($valor['coins_i'])){ echo $valor['coins_i'];} ?></td>
                                            <td><?php if(isset($valor['coins_f'])){ echo $valor['coins_f'];} ?></td>
                                            <td><?php if(isset($valor['time_f']) && $valor['time_i']){ $timen=(strtotime($valor['time_f']) - strtotime($valor['time_i']) )/ 60; echo round($timen,2);} ?>
                                        
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
